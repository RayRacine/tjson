#lang info
(define collection "tjson")
(define deps '("base" "typed-racket-more" "typed-racket-lib"))
(define build-deps '("scribble-lib" "sandbox-lib" "racket-doc" "rackunit-lib" "typed-racket-lib" "typed-racket-more" "typed-racket-doc"))
(define scribblings '(("scribblings/tjson.scrbl" ())))
(define pkg-desc "Straightforward Typed Json Parser and Emitter")
(define version "0.1.0")
(define pkg-authors '("Raymond Racine"))
